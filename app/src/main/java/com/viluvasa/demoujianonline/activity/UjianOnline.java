package com.viluvasa.demoujianonline.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.viluvasa.demoujianonline.R;
import com.viluvasa.demoujianonline.adapter.adapterAnswer;
import com.viluvasa.demoujianonline.bridge.AppConfig;
import com.viluvasa.demoujianonline.bridge.AppController;
import com.viluvasa.demoujianonline.model.ItemAnswer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UjianOnline extends AppCompatActivity {
    private static final String TAG = UjianOnline.class.getSimpleName();

    private List<ItemAnswer> ItemList = new ArrayList<ItemAnswer>();
    private ListView listView;
    private adapterAnswer adapter;
    TextView TvNomor, TvSoal;

    String kelas="11", kodpel="11";
    String JawabanSoal;

    int no_soal;
    int max_soal;
    int Nilai_Siswa;
    //data soal
    JSONArray soal;
    JSONObject obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ujian_online);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //listView start
        listView = (ListView) findViewById(R.id.listview_jawaban);
        adapter = new adapterAnswer(this, ItemList);
        listView.setAdapter(adapter);
        // Item Click Listener for the listview
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View container, int position, long id) {
                // Getting the Container Layout of the ListView
                RelativeLayout RelativeLayoutParent = (RelativeLayout) container;

                // Getting the inner Linear Layout
                //RelativeLayout RelativeLayoutChild = (RelativeLayout) RelativeLayoutParent.getChildAt(1);

                // Getting the Country TextView
                TextView pilgan = (TextView) RelativeLayoutParent.getChildAt(0);
                TextView soal = (TextView) RelativeLayoutParent.getChildAt(1);

                KlikJawab(pilgan.getText().toString());


            }
        };
        listView.setOnItemClickListener(itemClickListener);

        TvNomor = (TextView)findViewById(R.id.nomor);
        TvSoal = (TextView)findViewById(R.id.soal);
        no_soal=1;
        Nilai_Siswa = 0;
        LoadData(kelas, kodpel);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            //return true;
        }else if (id == R.id.action_refresh){
            no_soal=1;
            Nilai_Siswa = 0;
            LoadData(kelas, kodpel);

        }

        return super.onOptionsItemSelected(item);
    }

    private void LoadData(final String cls, final String kodpel) {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Soal, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        Toast.makeText(getApplicationContext(), "Sukses Load Data", Toast.LENGTH_SHORT).show();
                        soal = jObj.getJSONArray("soal");
                        max_soal = soal.length();
                        PasangSoal();
//                        for (int i = 0; i < soal.length(); i++) {
//                            try {
//
//                                JSONObject obj = data.getJSONObject(i);
////                                ItemHarian items = new ItemHarian();
////                                items.setCalender(obj.getString("tanggal"));
////                                items.setTotal(""+separator(obj.getString("total")));
//
//
//
////                                ItemList.add(items);
//
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//
//                        }
                    }else{
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                    // notifying list adapter about data changes
                    // so that it renders the list view with updated data
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getBaseContext(), "Gagal akses data. Periksa koneksi anda", Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("cls", cls);
                params.put("kodpel", kodpel);


                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }
    private void PasangList(int no){
        try {
            obj = soal.getJSONObject(no-1);

            TvNomor.setText(obj.getString("nomor"));
            TvSoal.setText(obj.getString("soal"));
            JawabanSoal = obj.getString("jawaban");
            ItemList.clear();

            for (int i = 1; i <= obj.length(); i++) {
                PasangBarisJawab(i);
            }


        }catch (JSONException e){

        }
        adapter.notifyDataSetChanged();
    }
    private void PasangBarisJawab(int number){
        try{
            ItemAnswer items = new ItemAnswer();
            if (number == 1){
                items.setPilihan("A");
                items.setIsiJawaban(obj.getString("pil_a"));
                ItemList.add(items);
            }else if (number == 2){
                items.setPilihan("B");
                items.setIsiJawaban(obj.getString("pil_b"));
                ItemList.add(items);
            }else if (number == 3){
                items.setPilihan("C");
                items.setIsiJawaban(obj.getString("pil_c"));
                ItemList.add(items);
            }else if (number == 4){
                items.setPilihan("D");
                items.setIsiJawaban(obj.getString("pil_d"));
                ItemList.add(items);
            }else if (number == 5){
                if (!(obj.getString("pil_e").equals("null"))){
                    items.setPilihan("E");
                    items.setIsiJawaban(obj.getString("pil_e"));
                    ItemList.add(items);
                }
            }


        }catch (JSONException e){

        }
    }
    private void PasangSoal(){
        if (max_soal >= no_soal){
            PasangList(no_soal);
            no_soal= no_soal+1;
        }else{
            ItemList.clear();
            TvSoal.setText("");
            TvNomor.setText("");
            Toast.makeText(getBaseContext(), "Selamat Hasil Nilai Anda : "+ Nilai_Siswa, Toast.LENGTH_SHORT).show();
        }


    }
    private void KlikJawab(final String jwb){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Perhatian")
                .setMessage("Apakah anda yakin dengan jawaban anda?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (JawabanSoal.equals(jwb)) {
                            //Toast.makeText(getBaseContext(), "Jawaban Benar : " +JawabanSoal, Toast.LENGTH_SHORT).show();
                            Nilai_Siswa = Nilai_Siswa + 10;

                            //Toast.makeText(getBaseContext(), "Nilai Anda : "+ Nilai_Siswa, Toast.LENGTH_SHORT).show();
                        }

                        PasangSoal();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }
}
