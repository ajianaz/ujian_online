package com.viluvasa.demoujianonline.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.viluvasa.demoujianonline.R;
import com.viluvasa.demoujianonline.adapter.adapterMapel;
import com.viluvasa.demoujianonline.bridge.AppConfig;
import com.viluvasa.demoujianonline.bridge.AppController;
import com.viluvasa.demoujianonline.model.ItemMapel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Mata_Pelajaran extends AppCompatActivity {
    private static final String TAG = UjianOnline.class.getSimpleName();

    private List<ItemMapel> ItemList = new ArrayList<ItemMapel>();
    private ListView listView;
    private adapterMapel adapter;

    String api_key = "viluvasa.com";
    String kodpel = "", nama_mapel = "";

    JSONArray MaPel;
    JSONObject obj;
    int Max_List;

    EditText TxtAddmapel;
    ImageView BtnAddmapel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mata_pelajaran);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TxtAddmapel = (EditText)findViewById(R.id.AddMapel);
        BtnAddmapel = (ImageView)findViewById(R.id.BtnAddMapel);
        BtnAddmapel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertAddMapel();
            }
        });

        //listView start
        listView = (ListView) findViewById(R.id.listview_mata_pelajaran);
        adapter = new adapterMapel(this, ItemList);
        listView.setAdapter(adapter);
        // Item Click Listener for the listview
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View container, int position, long id) {
                // Getting the Container Layout of the ListView
                RelativeLayout RelativeLayoutParent = (RelativeLayout) container;

                // Getting the inner Linear Layout
                //RelativeLayout RelativeLayoutChild = (RelativeLayout) RelativeLayoutParent.getChildAt(1);

                // Getting the Country TextView
                TextView kode = (TextView) RelativeLayoutParent.getChildAt(0);
                TextView nama = (TextView) RelativeLayoutParent.getChildAt(1);
                TextView nomor = (TextView) RelativeLayoutParent.getChildAt(2);

                kodpel = kode.getText().toString();
                nama_mapel = nama.getText().toString();

                TappedList();


            }
        };
        listView.setOnItemClickListener(itemClickListener);

        Get_Mapel(api_key);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main_admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            //return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //Ambil data Mata Pelajaran
    private void Get_Mapel(final String api) {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Get_MaPel, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        ItemList.clear();
                        //Toast.makeText(getApplicationContext(), "Sukses Load Data", Toast.LENGTH_SHORT).show();
                        MaPel = jObj.getJSONArray("mapel");
                        Max_List = MaPel.length();
                        PasangMapel();
                    }else{
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                    // notifying list adapter about data changes
                    // so that it renders the list view with updated data
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getBaseContext(), "Gagal akses data. Periksa koneksi anda", Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("apikey", api);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }
    //Edit Mata Pelajaran
    private void Edit_Mapel(final String api, final String mapel, final String kode) {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Edit_MaPel, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        Toast.makeText(getApplicationContext(),
                                jObj.getString("pesan"), Toast.LENGTH_LONG).show();
                        Get_Mapel(api_key);

                    }else{
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                    // notifying list adapter about data changes
                    // so that it renders the list view with updated data
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getBaseContext(), "Gagal akses data. Periksa koneksi anda", Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("apikey", api);
                params.put("mapel", mapel);
                params.put("kodpel", kode);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }
    //Hapus Mata Pelajaran
    private void Delete_Mapel(final String api, final String kode) {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Delete_MaPel, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        Toast.makeText(getApplicationContext(),
                               nama_mapel + jObj.getString("pesan"), Toast.LENGTH_LONG).show();
                        Get_Mapel(api_key);

                    }else{
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                    // notifying list adapter about data changes
                    // so that it renders the list view with updated data
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getBaseContext(), "Gagal akses data. Periksa koneksi anda", Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("apikey", api);
                params.put("kodpel", kode);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }


    //Memasang data ke list
    private void PasangList(int no){
        try {
            obj = MaPel.getJSONObject(no-1);

            ItemMapel item = new ItemMapel();
            item.setKode_mapel(obj.getString("kode_mapel"));
            item.setNama_mapel(obj.getString("nama_mapel"));
            item.setNo_mapel(obj.getString("nomor_mapel"));
            ItemList.add(item);


        }catch (JSONException e){

        }
        adapter.notifyDataSetChanged();
    }
    //Looping pemasangan List
    private void PasangMapel(){

        for (int j = 1; j <= Max_List; j++) {
            PasangList(j);
        }
    }
    //Menambahkan Alert Mata Pelajaran
    private void AlertAddMapel(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Perhatian")
                .setMessage("Tambahkan " + TxtAddmapel.getText().toString() + "?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Add_Mapel(api_key, TxtAddmapel.getText().toString().toUpperCase());
                        //Toast.makeText(getBaseContext(), TxtAddmapel.getText().toString()+" berhasil ditambahkan.", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }
    //Edit/Delete saat list di sentuh
    private void TappedList() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Perhatian")
                .setMessage(nama_mapel+"\nEdit or Delete ?")
                .setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AlertEdit_Mapel();
                        //Toast.makeText(getBaseContext(), "Edited", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ConfirmDeleteMapel();
                        // Toast.makeText(getBaseContext(), "Deleted", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }
    //konfirmasi hapus mata pelajaran
    private void ConfirmDeleteMapel(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Perhatian")
                .setMessage("Yakin hapus mata pelajaran "+nama_mapel+"?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Delete_Mapel(api_key,kodpel);
                        //Toast.makeText(getBaseContext(), "Terhapus", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }
    //Alert Edit Mapel
    private void AlertEdit_Mapel(){
        LinearLayout layout = new LinearLayout(getApplicationContext());
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText EditMapel = new EditText(getApplicationContext());
        EditMapel.setText(nama_mapel);
        EditMapel.setHintTextColor(getResources().getColor(R.color.bg_regis_login));
        EditMapel.setTextColor(getResources().getColor(R.color.hitam));

        layout.addView(EditMapel);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Edit "+nama_mapel)
                .setView(layout)
                .setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Edit_Mapel(api_key, EditMapel.getText().toString().toUpperCase(), kodpel);
                        //Toast.makeText(getBaseContext(), "Tersimpan", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Batal", null)
                .show();
    }
    //Menambahkan Mata Pelajaran
    private void Add_Mapel(final String api, final String mapel) {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Add_MaPel, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        Toast.makeText(getApplicationContext(),
                                jObj.getString("pesan"), Toast.LENGTH_LONG).show();
                        Get_Mapel(api_key);
                        TxtAddmapel.setText("");

                    }else{
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                    // notifying list adapter about data changes
                    // so that it renders the list view with updated data
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getBaseContext(), "Gagal akses data. Periksa koneksi anda", Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("apikey", api);
                params.put("mapel", mapel);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

}
