package com.viluvasa.demoujianonline.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.viluvasa.demoujianonline.R;
import com.viluvasa.demoujianonline.bridge.AppConfig;
import com.viluvasa.demoujianonline.bridge.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {
    private static final String TAG = UjianOnline.class.getSimpleName();
    EditText Email, Password;
    Button BtnLogin;

    String api_key = "viluvasa.com";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Email = (EditText)findViewById(R.id.Login_Email);
        Password = (EditText)findViewById(R.id.Login_Password);

        BtnLogin = (Button)findViewById(R.id.Login_BtnLogin);
        BtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login_Data(api_key, Email.getText().toString(), Password.getText().toString());
            }
        });

    }
    private void Login_Data(final String api, final String email, final String password) {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_member_Login, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        JSONObject user = jObj.getJSONObject("user");
                        String kode = user.getString("kode");

                        if (kode.equals("0")){
                            Toast.makeText(getApplicationContext(),
                                    "Login Member", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(getApplicationContext(), MainSiswa.class);
                            startActivity(intent);
                        }else {
                            Toast.makeText(getApplicationContext(),
                                    "Login Admin", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(getApplicationContext(), MainAdmin.class);
                            startActivity(intent);
                        }
                        //tutup Login Form
                        finish();
                    }else{
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                    // notifying list adapter about data changes
                    // so that it renders the list view with updated data


                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getBaseContext(), "Gagal akses data. Periksa koneksi anda", Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("apikey", api);
                params.put("sandi", password);
                params.put("email", email);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }
}
