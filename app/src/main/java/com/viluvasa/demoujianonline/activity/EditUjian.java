package com.viluvasa.demoujianonline.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.viluvasa.demoujianonline.R;
import com.viluvasa.demoujianonline.adapter.adapterUjian;
import com.viluvasa.demoujianonline.bridge.AppConfig;
import com.viluvasa.demoujianonline.bridge.AppController;
import com.viluvasa.demoujianonline.model.ItemUjian;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditUjian extends AppCompatActivity {
    private static final String TAG = UjianOnline.class.getSimpleName();

    private List<ItemUjian> ItemList = new ArrayList<ItemUjian>();
    private ListView listView;
    private adapterUjian adapter;

    String api_key = "viluvasa.com";
    String Gkodjian, Gmapel, Gkelas;

    JSONArray Daftar_Ujian;
    JSONObject obj;
    int Max_List;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_ujian);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), InputUjian.class);
                startActivity(intent);
            }
        });

        //listView start
        listView = (ListView) findViewById(R.id.listview_daftar_ujian);
        adapter = new adapterUjian(this, ItemList);
        listView.setAdapter(adapter);
        // Item Click Listener for the listview
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View container, int position, long id) {
                // Getting the Container Layout of the ListView
                RelativeLayout RelativeLayoutParent = (RelativeLayout) container;

                // Getting the inner Linear Layout
                //RelativeLayout RelativeLayoutChild = (RelativeLayout) RelativeLayoutParent.getChildAt(1);

                // Getting the Country TextView
                TextView kode = (TextView) RelativeLayoutParent.getChildAt(0);
                TextView mapel = (TextView) RelativeLayoutParent.getChildAt(1);
                TextView kelas = (TextView) RelativeLayoutParent.getChildAt(2);

                Gkodjian = kode.getText().toString();
                Gmapel = mapel.getText().toString();
                Gkelas = kelas.getText().toString();

                TappedList();
            }
        };
        listView.setOnItemClickListener(itemClickListener);

        Get_Daftar_Ujian(api_key);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            //return true;
        }else if (id == R.id.action_refresh) {
//            Intent intent = new Intent(getApplicationContext(), EditUjian.class);
//            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Refresh", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }
    //Ambil data Mata Pelajaran
    private void Get_Daftar_Ujian(final String api) {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Get_Ujian, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        ItemList.clear();
                        //Toast.makeText(getApplicationContext(), "Sukses Load Data", Toast.LENGTH_SHORT).show();
                        Daftar_Ujian = jObj.getJSONArray("data");
                        Max_List = Daftar_Ujian.length();
                        PasangDaftarUjian();
                    }else{
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                    // notifying list adapter about data changes
                    // so that it renders the list view with updated data
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getBaseContext(), "Gagal akses data. Periksa koneksi anda", Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("apikey", api);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    //Memasang data ke list
    private void PasangList(int no){
        try {
            obj = Daftar_Ujian.getJSONObject(no-1);

            ItemUjian item = new ItemUjian();
            item.setKode_ujian(obj.getString("kode_ujian"));
            item.setNama_mapel(obj.getString("nama_mapel"));
            item.setKelas(obj.getString("kelas"));

            ItemList.add(item);


        }catch (JSONException e){

        }
        adapter.notifyDataSetChanged();
    }
    //Looping pemasangan List
    private void PasangDaftarUjian(){

        for (int j = 1; j <= Max_List; j++) {
            PasangList(j);
        }
    }

    //Edit/Delete saat list di sentuh
    private void TappedList() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Perhatian")
                .setMessage("\nEdit or Delete ?")
                .setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), DaftarListSoal.class);
                        intent.putExtra("kodjian", Gkodjian);
                        intent.putExtra("mapel", Gmapel);
                        intent.putExtra("kelas", Gkelas);
                        startActivity(intent);
                        //Toast.makeText(getBaseContext(), "Edited", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ConfirmDelete();
                        // Toast.makeText(getBaseContext(), "Deleted", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }
    //konfirmasi hapus mata pelajaran
    private void ConfirmDelete(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Perhatian")
                .setMessage("Yakin hapus data ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //kode_ujian
                        //Delete(api_key,kodpel);
                        Toast.makeText(getBaseContext(), "Terhapus", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }
    private void Delete(final String api, final String kode) {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Delete_MaPel, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
//                        Toast.makeText(getApplicationContext(),
//                                nama_mapel + jObj.getString("pesan"), Toast.LENGTH_LONG).show();
//                        Get_Mapel(api_key);

                    }else{
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                    // notifying list adapter about data changes
                    // so that it renders the list view with updated data
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getBaseContext(), "Gagal akses data. Periksa koneksi anda", Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("apikey", api);
                params.put("kodpel", kode);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }
}
