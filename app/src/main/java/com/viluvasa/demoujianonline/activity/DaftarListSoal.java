package com.viluvasa.demoujianonline.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.viluvasa.demoujianonline.R;
import com.viluvasa.demoujianonline.adapter.adapterListSoal;
import com.viluvasa.demoujianonline.bridge.AppConfig;
import com.viluvasa.demoujianonline.bridge.AppController;
import com.viluvasa.demoujianonline.model.ItemListSoal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DaftarListSoal extends AppCompatActivity {
    private static final String TAG = UjianOnline.class.getSimpleName();

    private List<ItemListSoal> ItemList = new ArrayList<ItemListSoal>();
    private ListView listView;
    private adapterListSoal adapter;

    String api_key = "viluvasa.com";
    String Gkodjian, Gmapel, Gkelas;

    JSONArray DataSoal;
    JSONObject obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_list_soal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Gkodjian = getIntent().getStringExtra("kodjian");
        Gmapel = getIntent().getStringExtra("mapel");
        Gkelas = getIntent().getStringExtra("kelas");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), InputSoal.class);
                startActivity(intent);
            }
        });

        //listView start
        listView = (ListView) findViewById(R.id.listview_list_soal);
        adapter = new adapterListSoal(this, ItemList);
        listView.setAdapter(adapter);
        // Item Click Listener for the listview
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View container, int position, long id) {
                // Getting the Container Layout of the ListView
                RelativeLayout RelativeLayoutParent = (RelativeLayout) container;

                // Getting the inner Linear Layout
                //RelativeLayout RelativeLayoutChild = (RelativeLayout) RelativeLayoutParent.getChildAt(1);

                // Getting the Country TextView
                TextView kode = (TextView) RelativeLayoutParent.getChildAt(0);
                TextView nama = (TextView) RelativeLayoutParent.getChildAt(1);
                TextView nomor = (TextView) RelativeLayoutParent.getChildAt(2);

            }
        };
        listView.setOnItemClickListener(itemClickListener);
        Get_Data_Soal(api_key, Gmapel, Gkelas, Gkodjian);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main_admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            //return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //Ambil data Mata Pelajaran
    private void Get_Data_Soal(final String api, final String napel, final String kelas, final String kodjian) {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Get_List_Soal, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());


                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        ItemList.clear();
                        //Toast.makeText(getApplicationContext(), "Sukses Load Data", Toast.LENGTH_SHORT).show();
                        DataSoal = jObj.getJSONArray("data");

                        for (int i=0; i<DataSoal.length();i++){
                            obj = DataSoal.getJSONObject(i);

                            ItemListSoal item = new ItemListSoal();
                            item.setJawaban(obj.getString("jawaban"));
                            item.setNo_id(obj.getString("no"));
                            item.setSoal(obj.getString("soal"));
                            ItemList.add(item);
                        }
                        adapter.notifyDataSetChanged();
                    }else{
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                    // notifying list adapter about data changes
                    // so that it renders the list view with updated data
                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getBaseContext(), "Gagal akses data. Periksa koneksi anda", Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("apikey", api);
                params.put("napel", napel);
                params.put("kodjian", kodjian);
                params.put("kelas", kelas);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }
}
