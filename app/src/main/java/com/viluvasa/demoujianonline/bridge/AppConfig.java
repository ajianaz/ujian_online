package com.viluvasa.demoujianonline.bridge;

/**
 * Created by AJI on 3/12/2016.
 */
public class AppConfig {
    public static String Http = "http://";
    public static String Domain = "192.168.56.1";
    public static String folder = "ujianonline";

    public static String Base_Url = Http+Domain+"/"+folder;

    public static String URL_Soal = Base_Url + "/soal.php";

    //Mata Pelajaran
    public static String MAPEL = "mapel";
    public static String URL_Get_MaPel = Base_Url +"/" + MAPEL +"/Get_Mapel.php";
    public static String URL_Add_MaPel = Base_Url + "/" + MAPEL + "/Add_Mapel.php";
    public static String URL_Edit_MaPel = Base_Url + "/" + MAPEL + "/Edit_Mapel.php";
    public static String URL_Delete_MaPel = Base_Url + "/" + MAPEL + "/Delete_Mapel.php";

    //Member
    public static String MEMBER = "member";
    public static String URL_member_Login = Base_Url+ "/" + MEMBER + "/Login.php";

    //UJIAN
    public static String UJIAN = "ujian";
    public static String URL_Get_Ujian = Base_Url +"/" + UJIAN +"/Get_Ujian.php";

    //Soal
    public static String SOAL = "soal";
    public static String URL_Get_List_Soal = Base_Url +"/" + SOAL +"/get_soal.php";

}
