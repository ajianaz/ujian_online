package com.viluvasa.demoujianonline.model;

/**
 * Created by AJI on 3/11/2016.
 */
public class ItemAnswer {
    public String pilihan;
    public String IsiJawaban;

    public ItemAnswer() {

    }

    public String getIsiJawaban() {
        return IsiJawaban;
    }

    public String getPilihan() {
        return pilihan;
    }

    public void setIsiJawaban(String isiJawaban) {
        IsiJawaban = isiJawaban;
    }

    public void setPilihan(String pilihan) {
        this.pilihan = pilihan;
    }
}
