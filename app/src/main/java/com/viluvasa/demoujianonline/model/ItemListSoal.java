package com.viluvasa.demoujianonline.model;

/**
 * Created by AJI on 3/15/2016.
 */
public class ItemListSoal {
    public String no_id;
    public String soal;
    public String jawaban;

    public ItemListSoal(){

    }

    public String getJawaban() {
        return jawaban;
    }

    public String getNo_id() {
        return no_id;
    }

    public String getSoal() {
        return soal;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

    public void setNo_id(String no_id) {
        this.no_id = no_id;
    }

    public void setSoal(String soal) {
        this.soal = soal;
    }
}
