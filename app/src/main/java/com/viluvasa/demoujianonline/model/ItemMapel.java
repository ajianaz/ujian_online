package com.viluvasa.demoujianonline.model;

/**
 * Created by AJI on 3/12/2016.
 */
public class ItemMapel {
    public String no_mapel;
    public String nama_mapel;
    public String kode_mapel;

    public ItemMapel(){

    }

    public String getKode_mapel() {
        return kode_mapel;
    }

    public String getNama_mapel() {
        return nama_mapel;
    }

    public String getNo_mapel() {
        return no_mapel;
    }

    public void setKode_mapel(String kode_mapel) {
        this.kode_mapel = kode_mapel;
    }

    public void setNama_mapel(String nama_mapel) {
        this.nama_mapel = nama_mapel;
    }

    public void setNo_mapel(String no_mapel) {
        this.no_mapel = no_mapel;
    }

}
