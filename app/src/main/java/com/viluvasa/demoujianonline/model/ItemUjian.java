package com.viluvasa.demoujianonline.model;

/**
 * Created by AJI on 3/14/2016.
 */
public class ItemUjian {
    public String kode_ujian;
    public String nama_mapel;
    public String kelas;

    public ItemUjian(){

    }

    public String getNama_mapel() {
        return nama_mapel;
    }

    public String getKelas() {
        return kelas;
    }

    public String getKode_ujian() {
        return kode_ujian;
    }

    public void setNama_mapel(String nama_mapel) {
        this.nama_mapel = nama_mapel;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public void setKode_ujian(String kode_ujian) {
        this.kode_ujian = kode_ujian;
    }
}
