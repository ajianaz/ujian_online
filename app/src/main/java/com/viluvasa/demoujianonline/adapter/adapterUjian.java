package com.viluvasa.demoujianonline.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.viluvasa.demoujianonline.R;
import com.viluvasa.demoujianonline.bridge.AppController;
import com.viluvasa.demoujianonline.model.ItemUjian;

import java.util.List;

/**
 * Created by AJI on 3/14/2016.
 */
public class adapterUjian extends BaseAdapter{
    private Activity activity;
    private LayoutInflater inflater;
    private List<ItemUjian> Items;
    RequestQueue requestQueue = AppController.getInstance().getRequestQueue();

    public adapterUjian(Activity activity, List<ItemUjian> Item_Ujian){
        this.activity = activity;
        this.Items = Item_Ujian;
    }

    @Override
    public int getCount() {
        return Items.size();
    }

    @Override
    public Object getItem(int position) {
        return Items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_daftar_ujian, null);

        if (requestQueue != null){
            //requestQueue = AppController.getInstance().getRequestQueue();

            TextView kode_ujian = (TextView) convertView.findViewById(R.id.list_kode_ujian);
            TextView mapel_ujian = (TextView) convertView.findViewById(R.id.list_mapel_ujian);
            TextView kelas_ujian = (TextView) convertView.findViewById(R.id.list_kelas_ujian);

            // getting ItemsHarian data for the row
            ItemUjian IU = Items.get(position);

            // Kode Ujian
            kode_ujian.setText(IU.getKode_ujian());


            // Nama Mapel
            mapel_ujian.setText(IU.getNama_mapel());

            // kelas
            kelas_ujian.setText(IU.getKelas());
        }

        return convertView;
    }
}
