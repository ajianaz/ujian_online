package com.viluvasa.demoujianonline.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.viluvasa.demoujianonline.R;
import com.viluvasa.demoujianonline.bridge.AppController;
import com.viluvasa.demoujianonline.model.ItemListSoal;

import java.util.List;

/**
 * Created by AJI on 3/15/2016.
 */
public class adapterListSoal extends BaseAdapter{
    private Activity activity;
    private LayoutInflater inflater;
    private List<ItemListSoal> Items;
    RequestQueue requestQueue = AppController.getInstance().getRequestQueue();

    public adapterListSoal(Activity activity, List<ItemListSoal> Item_Soal){
        this.activity = activity;
        this.Items = Item_Soal;
    }

    @Override
    public int getCount() {
        return Items.size();
    }

    @Override
    public Object getItem(int position) {
        return Items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_soal_jawaban, null);

        if (requestQueue != null){
            //requestQueue = AppController.getInstance().getRequestQueue();

            TextView no_id = (TextView) convertView.findViewById(R.id.list_soal_id);
            TextView soal = (TextView) convertView.findViewById(R.id.list_soal_soalnya);
            TextView jawaban = (TextView) convertView.findViewById(R.id.list_soal_jawab);

            // getting ItemsHarian data for the row
            ItemListSoal IS = Items.get(position);

            // ID
            no_id.setText(IS.getNo_id());

            //soal
            soal.setText(IS.getSoal());

            //jawaban
            jawaban.setText(IS.getJawaban());
        }

        return convertView;
    }
}
