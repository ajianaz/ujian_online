package com.viluvasa.demoujianonline.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.viluvasa.demoujianonline.R;
import com.viluvasa.demoujianonline.bridge.AppController;
import com.viluvasa.demoujianonline.model.ItemAnswer;

import java.util.List;

/**
 * Created by AJI on 3/11/2016.
 */
public class adapterAnswer extends BaseAdapter{
    private Activity activity;
    private LayoutInflater inflater;
    private List<ItemAnswer> Items;
    RequestQueue requestQueue = AppController.getInstance().getRequestQueue();

    public adapterAnswer(Activity activity, List<ItemAnswer> Item_Answer){
        this.activity = activity;
        this.Items = Item_Answer;
    }

    @Override
    public int getCount() {
        return Items.size();
    }

    @Override
    public Object getItem(int position) {
        return Items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_jawaban, null);

        if (requestQueue != null){
            //requestQueue = AppController.getInstance().getRequestQueue();

            TextView pilihan = (TextView) convertView.findViewById(R.id.pilihan);
            TextView jawaban = (TextView) convertView.findViewById(R.id.IsiJawaban);

            // getting ItemsHarian data for the row
            ItemAnswer IA = Items.get(position);

            // Pilihan
            pilihan.setText(IA.getPilihan());

            // Jawaban
            jawaban.setText(IA.getIsiJawaban());

        }

        return convertView;
    }
}
