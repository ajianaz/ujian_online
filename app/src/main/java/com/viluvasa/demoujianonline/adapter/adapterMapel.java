package com.viluvasa.demoujianonline.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.viluvasa.demoujianonline.R;
import com.viluvasa.demoujianonline.bridge.AppController;
import com.viluvasa.demoujianonline.model.ItemMapel;

import java.util.List;

/**
 * Created by AJI on 3/12/2016.
 */
public class adapterMapel extends BaseAdapter{
    private Activity activity;
    private LayoutInflater inflater;
    private List<ItemMapel> Items;
    RequestQueue requestQueue = AppController.getInstance().getRequestQueue();

    public adapterMapel(Activity activity, List<ItemMapel> Item_Mapel){
        this.activity = activity;
        this.Items = Item_Mapel;
    }

    @Override
    public int getCount() {
        return Items.size();
    }

    @Override
    public Object getItem(int position) {
        return Items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_mata_pelajaran, null);

        if (requestQueue != null){
            //requestQueue = AppController.getInstance().getRequestQueue();

            TextView no_mapel = (TextView) convertView.findViewById(R.id.no_mapel);
            TextView nama_mapel = (TextView) convertView.findViewById(R.id.nama_mapel);
            TextView kode_mapel = (TextView) convertView.findViewById(R.id.kode_mapel);

            // getting ItemsHarian data for the row
            ItemMapel IM = Items.get(position);

            // Nomor
            no_mapel.setText(IM.getNo_mapel());


            // Nama
            nama_mapel.setText(IM.getNama_mapel());

            // kode
            kode_mapel.setText(IM.getKode_mapel());
        }

        return convertView;
    }
}
